if [[ -z "${1}" ]]; then
	exit
fi

vKey="${1}"

case "${vKey}" in
	"AWS_ACCESS_KEY_ID")
		echo "${AWS_ACCESS_KEY_ID}"
		;;
	"AWS_SECRET_ACCESS_KEY")
		echo "${AWS_SECRET_ACCESS_KEY}"
		;;
esac
