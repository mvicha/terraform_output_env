variable "region" {
  description = "Region where the instance is going to be deployed"
  default = "us-east-1"
}

#variable "access_key" {}

#variable "secret_key" {}
