module "aws_access_key_id" {
  source  = "matti/resource/shell"
  command = "/bin/bash outputs.sh AWS_ACCESS_KEY_ID"
}

module "aws_secret_access_key" {
  source = "matti/resource/shell"
  command = "/bin/bash outputs.sh AWS_SECRET_ACCESS_KEY"
}
