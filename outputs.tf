output "my_aws_access_key_id" {
  value = "${module.aws_access_key_id.stdout}"
}

output "my_aws_secret_access_key" {
  value = "${module.aws_secret_access_key.stdout}"
}
